package com.waffle.nutapostest.ui.home

import android.content.Intent
import android.text.method.TextKeyListener.clear
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.waffle.nutapostest.data.entity.MoneyEntity
import com.waffle.nutapostest.databinding.ItemHomeDateBinding
import java.util.Collections.addAll

class HomeDateAdapter : RecyclerView.Adapter<HomeDateAdapter.RecyclerViewholder>() {

    private var listData = listOf<HomeDate>()

    inner class RecyclerViewholder(private val binding: ItemHomeDateBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(date: HomeDate) {
            binding.apply {
                tvDate.text = date.date
                rvHomeDate.apply {

                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewholder {
        val itemBinding = ItemHomeDateBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return RecyclerViewholder(itemBinding)
    }

    override fun getItemCount(): Int {
        return this.listData.size
    }

    override fun onBindViewHolder(holder: RecyclerViewholder, position: Int) {
        val data = this.listData[position]
        holder.bind(data)
    }

    fun setData(dataList: List<HomeDate>) {
        this.listData = listOf()
        this.listData = dataList

        notifyDataSetChanged()
    }
}