package com.waffle.nutapostest.ui.addMoney

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.waffle.nutapostest.R
import com.waffle.nutapostest.data.entity.MoneyEntity
import com.waffle.nutapostest.databinding.ActivityAddMoneyBinding
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.Calendar

class AddMoneyActivity : AppCompatActivity() {
    private lateinit var binding : ActivityAddMoneyBinding

    private val viewModel : AddMoneyViewModel by inject()

    private var dateNumber = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddMoneyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.apply {
            val data = intent.getParcelableArrayListExtra(DATA) ?: ArrayList<MoneyEntity>()
            etDate.setOnClickListener {
                getDate()
            }
            btnSave.setOnClickListener{
                val money = MoneyEntity(
                    terimaDari = etMoneyIn.text.toString(),
                    keterangan = etDesc.text.toString(),
                    jumlah = etAmount.text.toString().toDouble(),
                    tanggal = etDate.text.toString(),
                    nomor = "UM/$dateNumber/${data.filter { it.tanggal == etDate.text.toString() }.size + 1}"
                )
                viewModel.postMoneyLocal(money)
                finish()
            }
        }
    }

    private fun getDate() {
        val calendar = Calendar.getInstance()
        var day = calendar.get(Calendar.DAY_OF_MONTH)
        var month = calendar.get(Calendar.MONTH)
        var year = calendar.get(Calendar.YEAR)
        val dateTime = Calendar.getInstance()
        DatePickerDialog(
            this,
            { view, year, month, day ->
                dateTime.set(year, month, day)
                val dateFormater = SimpleDateFormat("dd/MM/yyyy").format(dateTime.time)
                dateNumber = SimpleDateFormat("ddMMyy").format(dateTime.time)
                binding.etDate.setText(dateFormater)
            },
            year, month, day
        ).show()

    }

    companion object {
        const val DATA = "AddMoneyActivity.data"
    }
}