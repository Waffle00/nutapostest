package com.waffle.nutapostest.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.waffle.nutapostest.R
import com.waffle.nutapostest.data.entity.MoneyEntity
import com.waffle.nutapostest.databinding.ActivityHomeBinding
import com.waffle.nutapostest.ui.addMoney.AddMoneyActivity
import org.koin.android.ext.android.inject

class HomeActivity : AppCompatActivity() {
    private lateinit var binding : ActivityHomeBinding

    private val viewModel : HomeViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        observeData()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getMoneyLocal()
    }

    private fun observeData() {
        viewModel.observeGetMoneyLocalSuccess().observe(this) {
            it.getContentIfNotHandled().let {data ->
                val homeAdapter = HomeAdapter()
                binding.rvHome.apply {
                    adapter = homeAdapter
                    layoutManager = LinearLayoutManager(this@HomeActivity)
                }
                homeAdapter.setData(data ?: listOf())
                val arrayMoney = ArrayList<MoneyEntity>()
                    data?.map { money ->
                    arrayMoney.add(money)
                }
                binding.btnAddMoney.setOnClickListener {
                    startActivity(Intent(this, AddMoneyActivity::class.java).putExtra(AddMoneyActivity.DATA, arrayMoney))
                }
            }
        }
    }
}