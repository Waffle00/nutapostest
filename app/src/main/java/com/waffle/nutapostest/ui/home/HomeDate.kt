package com.waffle.nutapostest.ui.home

import android.os.Parcelable
import com.waffle.nutapostest.data.entity.MoneyEntity
import kotlinx.parcelize.Parcelize

@Parcelize
data class HomeDate(
    val date: String,
    val moneyList: List<MoneyEntity>
) : Parcelable
