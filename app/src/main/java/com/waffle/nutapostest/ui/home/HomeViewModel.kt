package com.waffle.nutapostest.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waffle.nutapostest.base.ResponseResult
import com.waffle.nutapostest.data.AppRepository
import com.waffle.nutapostest.data.entity.MoneyEntity
import com.waffle.nutapostest.utils.SingleLiveEvent
import kotlinx.coroutines.launch

class HomeViewModel(private val repository: AppRepository) : ViewModel() {

    private val getMoneyLocalSuccess = MutableLiveData<SingleLiveEvent<List<MoneyEntity>>>()

    fun observeGetMoneyLocalSuccess() : LiveData<SingleLiveEvent<List<MoneyEntity>>> = getMoneyLocalSuccess

    private val isError = MutableLiveData<SingleLiveEvent<String>>()

    fun observeIsError() : LiveData<SingleLiveEvent<String>> = isError

    fun getMoneyLocal() {
        viewModelScope.launch {
            when(val result = repository.getMoneyLocal()) {
                is ResponseResult.Success -> {
                    getMoneyLocalSuccess.postValue(SingleLiveEvent(result.data))
                }
                is ResponseResult.Error -> {
                    Log.e("TAG", "getMoneyLocal: ${result.errorMsg}", )
                    isError.postValue(SingleLiveEvent(result.errorMsg ?: ""))
                }
            }
        }
    }
}