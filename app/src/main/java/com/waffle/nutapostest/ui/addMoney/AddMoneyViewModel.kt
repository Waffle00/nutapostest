package com.waffle.nutapostest.ui.addMoney

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.waffle.nutapostest.data.AppRepository
import com.waffle.nutapostest.data.entity.MoneyEntity
import kotlinx.coroutines.launch

class AddMoneyViewModel(private val repository: AppRepository) : ViewModel() {

    fun postMoneyLocal(money: MoneyEntity) {
        viewModelScope.launch {
            repository.insertMoney(money)
        }
    }
}