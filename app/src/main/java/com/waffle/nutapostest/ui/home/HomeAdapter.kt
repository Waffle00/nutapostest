package com.waffle.nutapostest.ui.home

import android.text.method.TextKeyListener
import android.text.method.TextKeyListener.clear
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.waffle.nutapostest.data.entity.MoneyEntity
import com.waffle.nutapostest.databinding.ItemHomeBinding
import com.waffle.nutapostest.databinding.ItemHomeDateBinding
import java.util.Collections
import java.util.Collections.addAll

class HomeAdapter: RecyclerView.Adapter<HomeAdapter.RecyclerViewholder>() {

    private var listData = listOf<MoneyEntity>()

    inner class RecyclerViewholder(private val binding: ItemHomeBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(money: MoneyEntity ) {
            binding.apply {
                tvTitle.text = money.terimaDari
                tvAmount.text = money.jumlah.toString()
                tvDate.text = money.tanggal
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewholder {
        val itemBinding = ItemHomeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return RecyclerViewholder(itemBinding)
    }

    override fun getItemCount(): Int {
        return this.listData.size
    }

    override fun onBindViewHolder(holder: RecyclerViewholder, position: Int) {
        val data = this.listData[position]
        holder.bind(data)
    }

    fun setData(dataList: List<MoneyEntity>) {
        this.listData = listOf()
        this.listData = dataList
        notifyDataSetChanged()
    }
}