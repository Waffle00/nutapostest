package com.waffle.nutapostest.modules.room

import androidx.room.Room
import com.waffle.nutapostest.data.AppDataBase
import com.waffle.nutapostest.modules.room.RoomMigration.MIGRATION_1_2
import com.waffle.nutapostest.modules.room.RoomMigration.MIGRATION_2_3
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val roomModule = module {
    single {
        Room.databaseBuilder(androidContext(), AppDataBase::class.java, "db")
            .addMigrations(
                MIGRATION_1_2,
                MIGRATION_2_3
            )
            .fallbackToDestructiveMigration()
            .build()
    }
}
