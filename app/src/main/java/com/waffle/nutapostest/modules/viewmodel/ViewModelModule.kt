package com.waffle.nutapostest.modules.viewmodel

import com.waffle.nutapostest.ui.addMoney.AddMoneyViewModel
import com.waffle.nutapostest.ui.home.HomeViewModel
import org.koin.dsl.module


val viewModelModule = module {
    single { HomeViewModel(get()) }
    single { AddMoneyViewModel(get()) }
}


