package com.waffle.nutapostest.modules.room

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object RoomMigration {
    val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL(
                "CREATE TABLE IF NOT EXISTS money_db (uang_masuk_id INTEGER PRIMARY KEY NOT NULL, terima_dari TEXT, keterangan TEXT, jumlah DOUBLE)"
            )
        }
    }
    val MIGRATION_2_3 = object : Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL(
                "ALTER TABLE money_db ADD COLUMN tanggal TEXT"
            )
            database.execSQL(
                "ALTER TABLE money_db ADD COLUMN nomor TEXT"
            )
        }
    }
}