package com.waffle.nutapostest.modules.repository

import com.waffle.nutapostest.data.AppRepository
import org.koin.dsl.module

val repositoryModule = module {
    single {
        AppRepository(get())
    }
}