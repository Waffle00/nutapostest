package com.waffle.nutapostest.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.waffle.nutapostest.data.entity.MoneyEntity

@Dao
interface MoneyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMoney(money: MoneyEntity)

    @Query("SELECT * FROM money_db")
    suspend fun getMoneyLocal() : List<MoneyEntity>

    @Query("DELETE FROM money_db")
    suspend fun deleteMoney()
}