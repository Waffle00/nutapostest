package com.waffle.nutapostest.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.waffle.nutapostest.data.dao.MoneyDao
import com.waffle.nutapostest.data.entity.MoneyEntity

@Database(
    entities = [
        MoneyEntity::class
    ],
    version = 2,
    exportSchema = false
)
abstract class AppDataBase : RoomDatabase() {
    abstract fun moneyDao() : MoneyDao
}