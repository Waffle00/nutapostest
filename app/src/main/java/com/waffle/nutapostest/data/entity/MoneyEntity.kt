package com.waffle.nutapostest.data.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "money_db")
class MoneyEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "uang_masuk_id")
    val uangMasukId: Int = 0,
    @ColumnInfo(name = "terima_dari")
    val terimaDari: String?,
    @ColumnInfo(name = "keterangan")
    val keterangan: String?,
    @ColumnInfo(name = "jumlah")
    val jumlah: Double?,
    @ColumnInfo(name = "tanggal")
    val tanggal: String?,
    @ColumnInfo(name = "nomor")
    val nomor: String?
) : Parcelable {
    constructor() : this(
        uangMasukId = 0,
        terimaDari = null,
        keterangan = null,
        jumlah = null,
        tanggal = null,
        nomor = null
    )
}