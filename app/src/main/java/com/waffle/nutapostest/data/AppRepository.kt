package com.waffle.nutapostest.data

import com.waffle.nutapostest.base.ResponseResult
import com.waffle.nutapostest.data.entity.MoneyEntity

class AppRepository(
    private val appDataBase: AppDataBase
) {

    private suspend fun <T> getResult(
        request: suspend () -> T
    ): ResponseResult<T> {
        return try {
            val res = request.invoke()
            return ResponseResult.Success(res)
        } catch (e: Exception) {
            ResponseResult.Error(code = 404, errorMsg = e.message)
        }
    }

    suspend fun getMoneyLocal() : ResponseResult<List<MoneyEntity>> = getResult {
        appDataBase.moneyDao().getMoneyLocal()
    }

    suspend fun insertMoney(money: MoneyEntity) = appDataBase.moneyDao().insertMoney(money)
}