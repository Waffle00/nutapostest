package com.waffle.nutapostest

import android.app.Application
import com.waffle.nutapostest.modules.repository.repositoryModule
import com.waffle.nutapostest.modules.room.roomModule
import com.waffle.nutapostest.modules.viewmodel.viewModelModule
import org.koin.android.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@App)
            modules(listOf(repositoryModule, viewModelModule, roomModule))
        }
    }
}